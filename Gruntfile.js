module.exports = function(grunt) {
    
      // Project configuration.
      grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        qunit: {
            all: ['tests/*html']
        },
        qunit_junit: {
          options:{
            
          }
        },
        concat: {
            options: {
              separator: ';',
            },
            dist: {
              src: ['js/*.js'],
              dest: 'dist/built.js',
            },
        },        
        uglify: {
          options: {
            banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
          },
          build: {
            src: 'dist/built.js',
            dest: 'dist/built.min.js'
          }
        },

        watch:{
            scripts: {
                files: ['js/*.js'],
                tasks:['qunit','concat','uglify'],
                options: {
                    livereload: true,
                  }
            }
        }
      });
    
      // Load the plugin that provides the "uglify" task.
      grunt.loadNpmTasks('grunt-contrib-uglify');
      grunt.loadNpmTasks('grunt-contrib-concat');
      grunt.loadNpmTasks('grunt-contrib-qunit');
      grunt.loadNpmTasks('grunt-contrib-watch');
      grunt.loadNpmTasks('grunt-qunit-junit');
    
      // Default task(s).
      grunt.registerTask('default', ['qunit_junit','qunit','concat','uglify','watch']);
      grunt.registerTask('test', ['qunit_junit','qunit']);
      
    
    };