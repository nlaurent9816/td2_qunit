function validerNumeroSecu(numero){
    //test du type
    if(typeof numero != 'number')
        return false;
    //test de la longueur
    if(numero.toString().length !=15)
        return false;
    //test du premier numéro (1 ou 2)
    var sex = (numero-(numero%100000000000000))/100000000000000
    if(sex!=1 && sex!=2)
        return false;
    //test de la clé
    var cle = numero%100;
    var nss = (numero-cle)/100;
    if(cle !=(97-(nss%97)))
        return false;
    return true;

}